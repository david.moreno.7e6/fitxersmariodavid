import java.nio.file.Path
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import kotlin.io.path.*
import java.util.*
data class User(var id: String, var name: String, var phoneNum: String, var email: String, var active: Boolean, var blocked: Boolean)

fun main(args: Array<String>) {
    println("BIENVENIDO A ASMA FIT DATA MANAGER")
    imports()
    menu()
}
fun imports(){
    val userData= Path("./src/main/data/userData.txt")
    var import= Path("./src/main/import")
    val docs= import.listDirectoryEntries("*")
    if (docs.isEmpty()){
        println("No hay archivos que importar")
        menu()
    }
    println(docs[1])
    for (i in docs){
        var file= i
        val existFile= file.exists()
        var text: MutableList<String>
        var newUsers= mutableListOf<MutableList<String>>()
        if (existFile){
            text=(file.readText().split(";")).toMutableList()
            for (user in text){
                val data= user.split(",")
                var numero=-1
                var user= mutableListOf<String>()
                if(data[4]=="true" && data[5] == "false"){
                    for (i in data) {
                        numero++
                        if(numero!=0){
                            user.add("$i")
                        }
                    }
                }
                if (user.lastIndex== 4) {
                    newUsers.add(user)
                }
            }
            var id:Int
            for (user in newUsers){
                id= userData.readLines().lastIndex+2
                userData.appendText("$id;")
                for (datos in user){
                    userData.appendText("$datos;")
                }
                userData.appendText("\n")
            }
        }
        file.deleteIfExists()
    }
    println("Archivos importados correctamente")
    println("""
           
        
    """.trimIndent())
    menu()
}

fun createUser(){
    val userData= Path("./src/main/data/userData.txt")
    var user= mutableListOf<String>()
    val scan= Scanner(System.`in`)
    println("Nombre usuario:")
    var input= scan.nextLine()
    user.add(input)
    println("Telefono:")
    input= scan.nextLine()
    user.add(input)
    println("Email:")
    input= scan.nextLine()
    user.add(input)
    user.add("true")
    user.add("false")
    var id= userData.readLines().lastIndex+2
    userData.appendText("$id;")
    for (data in user){
        userData.appendText("$data;")
    }
    userData.appendText("\n")
    println("Usuario añadido a la base de datos")
    println("El id del usuario es $id")
    println("""
        
        
    """.trimIndent())
    menu()
}

fun updateUser(){
    val userData= Path("./src/main/data/userData.txt")
    println("Introduce el id del usuario:")
    val scan= Scanner(System.`in`)
    var id= scan.nextLine().toInt()
    if (id !in 1..userData.readLines().lastIndex+1 || userData.readLines()[0]== ""){
        println("No existe el usuario")
    }
    else {
        var user= (userData.readLines()[id-1].split(";")).toMutableList()
        val oldUser=userData.readLines()[id-1]
        var text= userData.readText().toString()
        println("Nombre usuario:")
        var input= scan.nextLine()
        user[1]=input
        println("Telefono:")
        input= scan.nextLine()
        user[2]=input
        println("Email:")
         input= scan.nextLine()
        user[3]=input
        var updatedUser=""
        for (data in 0 until user.lastIndex){
            updatedUser += "${user[data]};"
        }
        text=text.replace(oldUser,updatedUser)
        userData.writeText(text)
        println("Datos de usuario actualizados correctamente")
        println(updatedUser)
        println("""
            
            
        """.trimIndent())
    }
    menu()
}
fun unBlockOrBlockUser() {
    val userData = Path("./src/main/data/userData.txt")
    println("Introduce el id del usuario:")
    val scan = Scanner(System.`in`)
    var id = scan.nextLine().toInt()
    if (id !in 1..userData.readLines().lastIndex+1 || userData.readLines()[0]== ""){
        println("No existe el usuario")
    }else {
        var user = (userData.readLines()[id - 1].split(";")).toMutableList()
        val oldUser = userData.readLines()[id - 1]
        var text = userData.readText().toString()
        var valor= user[user.lastIndex-1]
        println("Usuario Bloqueado: ${user[user.lastIndex-1]}")
        println("Introduzca SI/NO para modificar o no el valor:")
        var correct= false
        while(correct==false){
            var input = scan.next()
            if(input.uppercase()=="SI"){
                if(valor.uppercase()=="TRUE"){
                    user[user.lastIndex-1]= "false"
                    println("Usuario Bloqueado: ${user[user.lastIndex-1]}")
                    println("""
                    
         
                """.trimIndent())
                    correct=true
                }
                else{
                    user[user.lastIndex-1]= "true"
                    println("Usuario Bloqueado: ${user[user.lastIndex-1]}")
                    println("""
                    
         
                """.trimIndent())
                    correct=true
                }

            }
            else if(input.uppercase()=="NO"){
                println("Usuario Bloqueado: ${user[user.lastIndex-1]}")
                println("""
                    
                      
                """.trimIndent())
                correct=true
            }
            else{
                println("RESPUESTA INCORRECTA")
                println("Introduzca SI/NO para modificar o no el valor:")
            }
        }
        var updatedUser = ""
        for (data in 0 until user.lastIndex ) {
            updatedUser += "${user[data]};"
        }
        text = text.replace(oldUser, updatedUser)
        userData.writeText(text)
    }
    menu()
}

fun doBackUp(){
    var format= DateTimeFormatter.ofPattern("dd-MM-yyyy")
    var day= LocalDate.now().format(format).toString()
    var backupToday= Path("./src/main/backup/${day}userData.txt")
    var updatedData= Path("./src/main/data/userData.txt")
    backupToday.writeText(updatedData.readText())
    menu()
}

fun exit(){
    var format= DateTimeFormatter.ofPattern("dd-MM-yyyy")
    var day= LocalDate.now().format(format).toString()
    var backupToday= Path("./src/main/backup/${day}userData.txt")
    var updatedData= Path("./src/main/data/userData.txt")
    if(backupToday.notExists()){
        doBackUp()
    }
}

fun menu(){
    println("Selecciona la operación a realizar:")
    println("1- Importar datos")
    println("2- Crear usuario")
    println("3- Modificar ususario")
    println("4- Des/bloquear usuario")
    println("5- Copia de seguridad")
    println("6- Exit")
    var correct= false
    while (correct==false){
        val scanner= Scanner(System.`in`)
        val input= scanner.nextInt()
        when(input){
            1-> imports()
            2-> createUser()
            3-> updateUser()
            4-> unBlockOrBlockUser()
            5-> doBackUp()
            6-> {
                exit()
                break
            }
            else-> {
                println("Opción no válida.")
                println("Selecciona una de las siguientes opciones:")
                println("1- Importar datos")
                println("2- Crear usuario")
                println("3- Modificar ususario")
                println("4- Des/bloquear usuario")
                println("5- Copia de seguridad")
                println("6- Exit")
            }
        }

    }
}